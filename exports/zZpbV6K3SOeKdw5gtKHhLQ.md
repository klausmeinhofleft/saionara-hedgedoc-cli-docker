# [rLab || Camino al Manifiesto] 
La propuesta es que dejemos **listas** de cosas que nos representan o queremos mencionar de la identidad del espacio y quienes lo habitan y construyen. 

Para esto haremos un manifiesto, siguiendo la técnica de cadaver exquisito: todxs dejarán sus palabras y una persona lo reformulará en un texto integrado final, cual manifiesto.

Si alguien quiere integrarse en esa fase 2 de producir el manifiesto usando técnicas como puesta en duda, desmitificación, interrogación rétorica, citas de autoridad, sarcasmos y otras yerbas, pongase en la lista final.

Base teórica:
> Qué es un manifiesto?
> • Es una manifestación explícita de la conciencia artística: un escrito en el que se hace publica una declaración o propósito de carácter general o mas especifico (declaración pública de principios estéticos, poéticos, y/o politicos, etc.)
> • Tiene carácter relacional, dialógico e intertextual
> • Es un Genero Discursivo de carácter meta-artistico / meta-discursivo
> • Denota conciencia de pertenencia y voluntad de intervención en un repertorio (conservación o modificación)
> • Suele tener un carácter catafórico (anticipatorio)
> • Genera, inventa, y pone en escena a un sujeto de enunciación
> (individual o colectivo)
> • Construye a un destinatario (al cual también apela)

Si les interesaría dejar algún parecer más libre-escrito sobre la identidad o filosofía que el lab les representa pueden escribirlo en la última sección.

*Sientase libre de expresarse.*

## 1. Listas
#### 1.1. FORMAS / COLORES / OLORES / SABORES / TEXTURAS / SONIDOS

mutable
negro
rojo
verde
violeta 
purpura
olor a cable quemado
pegajosa
dureza del hardware
placas
la voz de un poeta quebrando
glitch sonoro
constelacion
sintetizador
el chasquido de una lata de cerveza siendo abierta
repeticion
sample
ruido 
cartografía de tonos 
intensidades, velocidades y lentitudes 
vectores aberrantes 
afectividad Cyborg 
el led ultravioleta
sonido de tambores
olor y ruido de hojas secas
pixel
sonido a conglomeracion
reloj digital avanzando (aceleracionismo)
sonido de notificación

#### 1.2. VERBOS / ACCIONES
codear
pushear
cortar
empastar
pegar
desarmar
hackear
discutir
consensuar
samplear
romper
colaborar
librepensar
liberar
compartir
reordenar
reutilizar
reciclar
hablar [nuestra voz]
crear [artística y tecnológicamente]
hacer nuestra nube
apropiación
subir/uploadear/seedear
copiar, replicar, piratear, distribuir, enviar, recibir, transmitir
armar comunidad(es)
fraternizar
incluir [socialmente], invitar
querer <3
encriptar, securizar
transvalorar: referirse a una inversion de los valores dominantes que atraviesan los usos y consumos de las tecnologías
militar afectivamente
respetar
imaginar
dar
recibir
autocriticar
empatizar
militar
incluir
aprender
descubrir
probar
acelerar
commitear / comprometerse

#### 1.3. MATERIALES / TECNICAS / TECNOLOGIAS
plasticola
placas
cables
encriptar
codigos y convivencia
autogestion
software libre
cables de red
rj45
módems, rúters, placas de red, servidores
bits
teclas, teclados, botones, perillas, plaquetas, coolers
torrents, pdfs, ebooks, mp3, descarga directa
navegadores, addons [de navegador], [sistemas operativos] linux
collage 
intervención
colectivizar al autor o hacer visible que el autor es colectivo
articular (con otras orgas)
difundir
bots
identidad colectiva (un nombre generico para todes)
autoformacion
economias alternativas
codigo generativo
arte texto
ASCII

## 2. REFERENCIAS
#### ARTISTICAS / TEORICAS / IDEOLOGICAS
Libertad de Expresion
Alexandra Elbakyan
VNS Matrix 
Noisebridge
Minimalismo
Aaron Swartz
Sound Art
Carl Sagan
Cory Doctorow
Zizek
Snowden
Chelsea Manning
Assange(?)
Robert Lippok
Bookchin
Microsound (Music scene)
Ocalan
Morozov
TRRUENO
Conquistar el pan
Ryoji Ikeda
Ninja Tune (Record Label)
Raster Records
Ernesto Romeo
Jorge Haro
Antonio Negri
Saul Alinsky
desobediencia tecnologica
Carsten Nicolai
Autechre
трип (Trip Recordings)
librenauta
collabe & b <3
endefensadelsl
privacidad, intimidad
software libre
Tiqqun 
Haraway 
post-punk (?) 
música industrial
Norbert Elias
David Graeber
Ursula K Le Guin
Aurelio Voltaire
Starhawk
UTT
Movimiento kurdo
William Gibson
Marx
Chiptune
Eric Sadin
Lohana Berkins
OpenBSD
Greta Thunberg (?)
Mark Fisher (realismo capitalista)
Alex Williams & Nick Snircek (manifiesto aceleracionista)
Contrapoints / Natali Wynn
Thought Slime
YUGOPNIK
Hassan Abi


## 3. ANTI-REFERENCIAS
#### ARTISTICAS / TEORICAS / IDEOLOGICAS
Elon Musk
Steve Bannon
Mateo Salvatto
Santi Siri
Ayn Rand
Meritocracia
Jordan Peterson
Ben Shapiro
Agustin Laje
Libertarismo de Derecha | AnCap
Disney
capitalismo
individualismo
vigilancia masiva
cosas que son solo open source pero no son software libre
autoritarismo
fascismo
software privativo/propietario
juntar datos ajenos, vender datos
Mercado Libre
Google
Amazon Web Services
Sociedad Rural Argentina
Iglesia Catolica
Intelectuales funcionales al poder

## 4. PREGUNTAS
Seremos robots dominados por el monopolio tech?
Estoy en medio de una tecnocracia, ahora que hago?
ya podríamos considerarnos cyborgs?
copiar y compartir es robar?
quiénes tienen acceso a la tecnología?
quién está detrás de las aplicaciones que usás?
realmente creés que esa aplicación es "gratis"? que no la estás en realidad pagando con algo de valor? #estánArmandoPerfiles
¿qué son y cuáles son los efectos de un mundo de virtualidades? 
¿las tecnologías progresan o inventan mundos posibles? 
¿qué es "el progreso"? ¿y si mejor pensamos en lo progresivo de las formas y la materia?
El capital ya extendio su dominio total sobre la infra de Internet, creo subjetividades acordes en las redes sociales y es demasiado tarde para pensar cualquier emancipacion radical?
Si estamos incorporando machine learning a cualquier aspecto de la vida social, el cual se basa en aprendizaje inductivo, que espacio va a haber para las alteridades? cuan regulado va a ser la subjetividad y costoso la "anomalia", entre algoritmos y regulacion corporativa y estatal?
quienes estan consumiendo la data de nuestras caras en las camaras de reconocimiento facial del CABA? 
Por que el poder se apodera de nosotros, nos conoce mas que nosotres mismos y como sociedad no podemos siquiera formularlo como un problema a regular?
Como producimos otro internet?
El capital cada vez necesita menos del estado para regular(nos)?


## 5. AUDIENCIA
Orgas
personas interesadas que se sienten excluidas de los espacios convencionales de tecnología

comunidades orientadas a la reapropiacion de la tecnologia +2

publico orientado al arte sonoro
comunidades|compañias orientadas a la creacion de instrumentos electronico|digitales

interesadxs en pensar nuevas formas fe colectivizacion del saber y de las prácticas creativas 

quienes quieran otra forma de producir en la industria tech
quienes piensen que la tecnologia no es algo dado, un artefacto cerrado desprendido de lo social 
quienes actualmente o potencialmente quieren una relacion con la tecnologia por el lado de la creacion y no por el consumo
colectivas que hagan una lectura politica (en sentido amplio) de la tecnologia


## 6. EL AHORA
Pandemia global aceleracionista
Renacer, reformularnos, refactor 
coronaoportuncrisis
consecuencias de la vigilancia corporativa
tomar conciencia de los procesos con los cuales se produce el software, en terminos eticos y de seguridad
combatir o apropiarse luciferinamente de la colonización tecnocapitalista de la imaginación
cambio climático, territorios y recursos naturales en disputa


## 7. COMUNIDADES O NODOS CON LOS QUE COLABORAR
afines:
- cheLA | CABA chela.org.ar facebook.com/cheLA.exACTA
- flashparty / rebelion.digital ++++ son amigues
- Cybercirujas ++++ son amigues
- ToyLab | CABA **¿url?**
- I.3D **¿url?** | CC IMPA La Fábrica, CABA (·) facebook.com/ccimpalafabrica [activo, clases de guitarra/quichua/danza]
- Lista de hackerspaces wiki.hackerspaces.org/List_of_ALL_Hacker_Spaces

no tanto:
- Córdoba Hackerspace cordobahackerspace.org twitter.com/cbahackerspace [activo, infosec, ekoparty]
- 404Zone | Zona Oeste (·) - twitter.com/404zone facebook.com/404zone [inactivo, comunidad, meetups, nerdearla]
- Hackspace Litoral | Santa Fe twitter.com/HSlitoral facebook.com/hslitoral [inactivo]
- Wazzabi/BRC | Río Negro (·) - facebook.com/pg/WzzBrc facebook.com/groups/wazzabibrc [inactivo]
- Wazzabi | Río Negro facebook.com/Wazzabi twitter.com/wazzabi_ [inactivo, maker]
- Club de Reparadores | Itinerante facebook.com/ClubDeReparadores http://reparadores.club [activo, ambientalista, reparan de todo]
- NETI Makerspace | CABA twitter.com/netimaker [activo, emprendedurismo]
- Fab Lab Bariloche facebook.com/fablabbrc [activo, talleres, emprendedurismo]

Referencias:
(·) Significa que estamos en contacto y esperando su respuesta.

## 8. SECCIÓN DE TEXTOS LIBRES
el hacklab es un lugar de encuentro, de creación, de efervescencia cultural y de camaradería; un lugar hospitalario, de inclusión; de dicusión, reflexión y aprendizaje colectivo; espontáneo, libre y sin jefxs

# FASE 2
Gente que se ofrece a juntar todo y hacer una linda ensalada:
- solar
- franz
- serious
- jules 