FROM alpine

RUN apk update && apk upgrade
RUN apk add --no-cache git bash curl

# https://github.com/hedgedoc/cli#install
RUN git clone https://github.com/hedgedoc/cli hedgedoc-cli

RUN cp hedgedoc-cli/bin/hedgedoc /usr/local/bin/
