# Usando HedgeDoc-Cli
hedgedoc-cli https://github.com/hedgedoc/cli

Buildear la imagen:
`docker build -t hedgedoc-scrapper .`

Probar que ande el cli:
`docker run -it --rm hedgedoc-scrapper hedgedoc help`

Traer data de un pad particular:
`docker run -it --rm -e PAD_ID=zZpbV6K3SOeKdw5gtKHhLQ -e HEDGEDOC_SERVER='https://demo.hedgedoc.org' hedgedoc-scrapper sh -c 'hedgedoc export --md $PAD_ID && cat $PAD_ID.md'`

Descargar la data a carpeta `exports` (creada automáticamente):
```
docker run -it --rm -v `pwd`/exports:/exports -e PAD_ID=zZpbV6K3SOeKdw5gtKHhLQ -e HEDGEDOC_SERVER='https://demo.hedgedoc.org' hedgedoc-scrapper sh -c 'hedgedoc export --md $PAD_ID /exports/$PAD_ID.md'
```
